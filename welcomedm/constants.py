from enum import IntEnum

KEY_DM_ENABLED = "dmEnabled"
KEY_LOG_JOIN_ENABLED = "logJoinEnabled"
KEY_LOG_JOIN_CHANNEL = "logJoinChannel"
KEY_LOG_LEAVE_ENABLED = "logLeaveEnabled"
KEY_LOG_LEAVE_CHANNEL = "logLeaveChannel"
KEY_TITLE = "title"
KEY_MESSAGE = "message"
KEY_IMAGE = "image"
KEY_GREETINGS = "greetings"
KEY_RETURNING_GREETINGS = "returningGreetings"
KEY_WELCOME_CHANNEL = "welcomeChannel"
KEY_WELCOME_CHANNEL_ENABLED = "welcomeChannelSet"
KEY_DESCRIPTIONS = "descriptions"
KEY_JOINED_USER_IDS = "joinedUserIds"

MAX_MESSAGE_LENGTH = 2000
MAX_DESCRIPTION_LENGTH = 500

DEFAULT_GUILD = {
    KEY_DM_ENABLED: True,
    KEY_LOG_JOIN_ENABLED: False,
    KEY_LOG_JOIN_CHANNEL: None,
    KEY_LOG_LEAVE_ENABLED: False,
    KEY_LOG_LEAVE_CHANNEL: None,
    KEY_TITLE: "BeeHive - Server Security",
    KEY_MESSAGE: "<:check:937496235047276634> **WARNING!** <:check:937496235047276634>\n\n**You have joined a server protected by BeeHive.**\n\nBeeHive uses artificial intelligence-based API's to provide automatic moderation to servers of any scale.\n\nAs a result, depending on the server owner's settings, **your messages may be monitored**. Server staff are able to **recall your messages** at **any** time with **no notification**.\n\nBy remaining in the server, you agree to follow all server rules and to abide by Discord Terms of Service.\n\n**You will be globally banned for violating Discord Terms of Service, and exhibiting malicious behavior.\n\nTo read the full Privacy Policy, please visit https://cutt.ly/beehiveprivacypolicy**",
    KEY_IMAGE: None,
    KEY_GREETINGS: {},
    KEY_RETURNING_GREETINGS: {},
    KEY_WELCOME_CHANNEL: None,
    KEY_WELCOME_CHANNEL_ENABLED: False,
    KEY_DESCRIPTIONS: {},
    KEY_JOINED_USER_IDS: [],
}


class GreetingPools(IntEnum):
    DEFAULT = 0
    RETURNING = 1
