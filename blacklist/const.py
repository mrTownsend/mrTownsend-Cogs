"""Different constants for this cog"""

from typing import Any, Dict

_config_structure: Dict[str, Dict[str, Any]] = {
    "global": {
        "blacklist": {},
        "whitelist": {},
    },
    "guild": {
        "blacklist": {},
        "whitelist": {},
    },
}

__authors__ = ["Jojo#7791"]
__version__ = "2.0.0"
