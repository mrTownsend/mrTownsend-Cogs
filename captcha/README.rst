.. _captcha:

=======
CAPTCHA
=======

Through this guide, ``[p]`` will always represent your prefix. Replace
``[p]`` with your own prefix when you use these commands in Discord.

--------------
About this cog
--------------

This is a CAPTCHA system for Discord, designed to help stop raiders, advertising/spam/nitro bots, jacked and automated accounts, and more. 

--------
Commands
--------

Here are all the commands included in this cog (15):

* ``[p]ownersetcaptcha``
 Set options for the Captcha cog.
* ``[p]ownersetcaptcha setlog <logging_level>``
 Set the logging level of the cog.
* ``[p]setcaptcha``
 Configure Captcha in your server.
* ``[p]setcaptcha allowedretries <number_of_retry>``
 Set the number of retries allowed before getting kicked.
* ``[p]setcaptcha autorole``
 Set the roles to give when passing the captcha.
* ``[p]setcaptcha autorole add [roles...]``
 Add a role to give.
* ``[p]setcaptcha autorole list``
 List all roles that will be given.
* ``[p]setcaptcha autorole remove [roles...]``
 Remove a role to give.
* ``[p]setcaptcha channel <text_channel_or_'dm'>``
 Set the channel where the user will be challenged.
* ``[p]setcaptcha enable <true_or_false>``
 Enable or disable Captcha security.
* ``[p]setcaptcha forgetme``
 Delete guild's data.
* ``[p]setcaptcha logschannel <text_channel_or_'none'>``
 Set a channel where events are registered.
* ``[p]setcaptcha temprole <temporary_role_or_'none'>``
 Give a temporary role when initilalizing the captcha challenge.
* ``[p]setcaptcha timeout <time_in_minutes>``
 Set the timeout before the bot kick the user if the user doesn't answer.
* ``[p]setcaptcha type <type_of_captcha>``
 Change the type of Captcha challenge.

------------
Installation
------------

If you haven't added my repo before, lets add it first. We'll call it
"mrTownsend-Cogs" here.

.. code-block:: ini

    [p]repo add mrTownsend-Cogs https://github.com/mrTownsend/mrTownsend-Cogs

Now, we can install Captcha.

.. code-block:: ini

    [p]cog install mrTownsend-Cogs captcha

Once it's installed, it is not loaded by default. Load it by running the following
command:

.. code-block:: ini

    [p]load captcha

