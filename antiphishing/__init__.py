from .antiphishing import AntiPhishing


def setup(bot):
    cog = AntiPhishing(bot)
    bot.add_cog(cog)
