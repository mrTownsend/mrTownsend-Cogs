## My utils

Hello there! If you're contributing or taking a look, everything in this folder
is synced from a master repo at https://github.com/Vexed01/vex-cog-utils by GitHub Actions -
so it's probably best to look/edit there.

---

Last sync at: 2022-01-12 17:27:36 UTC

Version: `2.4.0`

Commit: [`ede8ed97e7b9b82c707f0496655ded9b497228f2`](https://github.com/Vexed01/vex-cog-utils/commit/ede8ed97e7b9b82c707f0496655ded9b497228f2)
