# mrTownsend-Cogs


 IF YOU ARE ONE OF THE DEVELOPERS WHOSE WORK IS LISTED HERE, CHILL OUT. READ THIS FIRST.

 As I've had to clarify to multiple people this was me ripping/backing up repos I use and it didn't drag anything but the source folders as configured. Sorry, just create an issue like everybody else. I have no problem giving credit where it is due. Just don't hassle me if you abandoned the project publicly a long time ago.

 > Customized Cogs and Specialized WARDEN Scripts for BeeHive Bot, powered by Red-DiscordBot

 Hi! First off - you may look thru these cogs and say "damn, that looks familiar". Yes. Many of the cogs here have been originally created by others, but are
 either underdeveloped in my opinion or have poor command documentation, spelling, command descriptions, syntax, etc. For **my** personal bot, I want
 things to look how I want them to look and work how I want them to work, and this repo stands as a collection of all the cogs that I'll be working on from time to time.

 If any of you fellow devs see this and want a credit tag in the readme by all means ask. As I've had to clarify to multiple people this was me ripping/backing up repos I use and it didn't drag anything but the source folders as configured. Sorry, just create an issue like everybody else. I have no problem giving credit where it is due. Just don't hassle me if you abandoned the project publicly a long time ago.

 Second off - under warden-scripts you will find scripts for use with the X26-Cogs module "Defender". Anything under that category has been tested to work and install correctly with Defender on an ongoing basis.
 You are free to modify these rules however you feel, HOWEVER, if you modify them don't ask me for help. If you change things - they might work better for **your** server in particular. Or you could break them. Understand that some of these warden rules can interface with your userbase in very dangerous, permanent, drastic ways and a mistake on your part could at the worst, result in mass deletion of channels, mass banning of users, or all sorts of other errors and accidents. If you're not sure what you are doing, stop doing it until you feel that's changed.

 # Cogs

 Cogs are as follows.

 **antiphishing** - Removes known bad links for "Free discord nitro" among many others of things. Is enabled on bot load by default with this version of the module, but does not set a default log channel. Silent protection is the intention behind this one.

 **backup** - Local utility, lets you backup servers and channels. Use this responsibly and always encrypt user data.

 **blacklist** - Handles blacklisting users from using the bot locally or globally. This is branded for BeeHive now, so you probably will need to change this before using it. If you don't need to, then congrats.

 **captcha** - Originally by [kreusada](http://github.com/kreusada) - this cog allows you to halt your users at the door and require them to complete either a plain text, basic image, or advanced image captcha. You are able to assign a privileged role on CAPTCHA completion, or removal of a privilege-restricting role.

**ccrole** - Interactive custom role creation.

**clock** - Create voice channels that display and automatically update the time in any timezone.

**decancer** -Originally by [phenom4n4n](https://github.com/phenom4n4n/phen-cogs) -  Automatically change user's names when they join and have a bunch of bullshit in their name.

**disboard** Originally by [phenom4n4n](https://github.com/phenom4n4n/phen-cogs) - Handles bump reminders, channel cleaning and locking, and more.

**emailverification** Originally by [flare](https://github.com/flaree) - Sort of like CAPTCHA, but requires your user to provide a code that they're emailed instead of solving a visual CAPTCHA code. About as equally as effective of an anti-bot solution.

**logging** - logging module, server join/leave/edit, user ban/kick/mute, channel edits, etc. The usual comprehensive logging module.

**imagescanning** - Interfaces with SightEngine to scan for Nudity, Offensive Content, Drugs, Weapons, Alcohol, Scam Content, and more. Can automatically delete, notify staff, etc.

**infochannel** - Channels with information about the server like member count, role count, stats, etc.

**invite** - Internal module that gives our users a pretty invite when they invite our bot.

**maintenance** - Control's the bot's maintenance status.

**reacticket** - Reactions-based ticketing system.

**userinfo** - Improved userinfo command.




 # WARDEN Scripts

 Warden scripts are as follows:

 **antiactivityadvertisement** - Automatically kicks users when a pre-defined advertisement is detected in their user activity when they join the server. Posts to staff notification channel and posts a mod-log entry.

 **antiblankprofilepicture** - Simple rule that kicks users that try to join and are still using the default Discord profile picture. Posts to staff notification channel and posts a mod-log entry.

 **antiemojispam** - Stops users spamming and/or raiding using emojis. Takes custom emojis into account. Posts to staff notification channel with evidence, and posts mod-log entries.

 **antilink** - Stops link usage from non-staff, non-trusted accounts, and non-helper accounts. Posts to staff notification channel with evidence, and posts mod-log entries.

 **antiuntrustedaccount** - Combo rule that takes multiple actions against new accounts when they do certain wierd things, like joining and sharing an invite immediately, being a new account, and other misc. things as I think of them.

 **antiemojispambanner** - this rule is used alongside "antiemojispam" to enable autobanning for users who do it excessively. Install this rule to enable banning. Remove it to disable banning.

 **antiemojispammuter** - this rule is used alongside "antiemojispam" to enable automuting for users who do it excessively. Install this rule to enable muting. Remove it to disable muting. Do not use both "antiemojispammuter" **and** "antiemojispambanner" - use one or the other.

 **antilinkmuter** - this rule is used alongside "antilink" to enable automuting for users who do it excessively. Install this rule to enable muting. Remove it to disable muting.

 **antimassmention** - Automatically delete attempts to mass mention users.

 **antimassmentionbanner** - this rule is used alongside "antimassmention" to enable autobanning for users who do it excessively. Install this rule to enable banning. Remove it to disable banning.

 **antimassmentionmuter** - this rule is used alongside "antimassmention" to enable automuting for users who do it excessively. Install this rule to enable muting. Remove it to disable. Do not use both "antimassmentionbanner" **and** "antimassmentionmuter". Use one or the other for best results.

 **antimassroleping** - Automatically delete attempts to mass mention roles by non-staff.

 **antimassrolepingmuter** - this rule is used alongside "antimassroleping" to enable automuting for users who do it excessively. Install this rule to enable muting. Remove it to disable.

 **antinitroscam** - Automatically detect and delete nitro phishing attempts. Sends a warning message in the channel and as a bonus sends the bot account a message telling them to screw off.
