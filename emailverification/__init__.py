from .emailverification import EmailVerification


async def setup(bot):
    cog = EmailVerification(bot)
    bot.add_cog(cog)
