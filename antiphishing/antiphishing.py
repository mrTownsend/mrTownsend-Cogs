import json
import logging
import re
from urllib.parse import urlparse

import aiohttp
import discord

from redbot.core import Config, checks
from redbot.core.commands import commands

log = logging.getLogger(name="red.mrtownsend.antiphishing")

DEFAULT_GUILD_CONFIG = {
    "log_channel": None,
    "should_delete": True,
    "public_alerts": None,
}

DOMAIN_LIST_URL = "https://bad-domains.walshy.dev/domains.json"


class AntiPhishing(commands.Cog):
    """Checks against a list of bad domains and reports phishing attempts"""

    def __init__(self, bot, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bot = bot
        self.config = Config.get_conf(self, identifier=280730525960896513)
        self.config.register_guild(**DEFAULT_GUILD_CONFIG)
        self.domain_list = None

    @checks.admin_or_permissions(manage_guild=True)
    @commands.group(name="antiphishing")
    async def antiphishing_group(self, ctx):
        pass

    @antiphishing_group.command(name="publicalerts")
    async def publicalerts_command(self, ctx, channel: discord.TextChannel = None):
        "Set the public alerts channel to keep members aware when possible phishing attacks are afoot"
        if channel is None:
            await self.config.guild(ctx.guild).public_alerts.set(None)
            return await ctx.send("I've removed the public alerts channel, disabling alerts.")

        await self.config.guild(ctx.guild).public_alerts.set(channel.id)
        return await ctx.send(f"👍 I will send public alert messages to {channel}.")

    @antiphishing_group.command(name="log")
    async def log_command(self, ctx, channel: discord.TextChannel = None):
        """Set the log channel

        Passing no channel will disable logging by removing the channel.
        """
        if channel is None:
            await self.config.guild(ctx.guild).log_channel.set(None)
            return await ctx.send("I've removed the log channel, this has disabled log messages.")

        await self.config.guild(ctx.guild).log_channel.set(channel.id)
        return await ctx.send(f"👍 I will send log messages to {channel}.")

    @antiphishing_group.command(name="delete")
    async def delete_command(self, ctx, should_delete: bool):
        """
        Set whether or not to delete a message when found.

        should_delete takes in bools: `True` or `False`
        """
        await self.config.guild(ctx.guild).should_delete.set(should_delete)
        return await ctx.send(
            f"I will {'delete messages' if should_delete else 'not do anything'}, when a bad domain is found."
        )

    @antiphishing_group.command(name="settings")
    async def settings_command(self, ctx):
        """
        Show current settings.
        """
        should_delete = await self.config.guild(ctx.guild).should_delete()
        log_channel = await self.config.guild(ctx.guild).log_channel()
        public_alerts = await self.config.guild(ctx.guild).public_alerts()

        if log_channel:
            tm = ctx.guild.get_channel(log_channel)
            if isinstance(tm, discord.TextChannel):
                log_channel = tm

        embed = discord.Embed(title="AntiPhishing Settings")
        embed.add_field(name="AutoDeletion", value=should_delete, inline=False)
        channel_value = f"{log_channel.mention} - `{log_channel.id}`" if log_channel else "**DISABLED**"
        embed.add_field(name="Logging Channel", value=channel_value, inline=False)

        if public_alerts:
            tm2 = ctx.guild.get_channel(public_alerts)
            if isinstance(tm2, discord.TextChannel):
                public_alerts = tm2
            channel_value2 = f"{public_alerts.mention} - `{public_alerts.id}`" if public_alerts else "**DISABLED**"
            embed.add_field(name="Alerts Channel", value=channel_value2, inline=False)

        await ctx.send(embed=embed)

    @commands.Cog.listener(name="on_message_without_command")
    async def on_message_listener(self, message: discord.Message):
        """Handles the messages incoming, transforms them and sends them for evaulation."""
        if message.author.bot:
            return

        if not message.guild:
            return

        if await self.bot.is_automod_immune(message.author):
            return

        list_of_links = re.findall(r'(https?://[^\s]+)', message.clean_content.lower())

        # parse the url and just use .netloc
        list_of_links = [urlparse(url).netloc for url in list_of_links]

        if not list_of_links:
            return

        try:
            contains_bad_domain = await self.contains_bad_domain(list_of_links)
            if contains_bad_domain:
                await self.handle_bad_domain(message)
        except RuntimeError as e:
            # lets just fail silently.
            log.exception(e)
            return

    async def contains_bad_domain(self, clean_content):
        if self.domain_list is None:
            await self.populate_domain_list()

        for domain in self.domain_list:
            if domain in clean_content:
                return True

    async def populate_domain_list(self):
        async with aiohttp.ClientSession() as session:
            async with session.get(url=DOMAIN_LIST_URL) as resp:
                if resp.status == 200:
                    resp_json = await resp.json()
                    self.domain_list = resp_json
                else:
                    raise RuntimeError("Api returned bad status.")

    async def handle_bad_domain(self, message):
        should_delete = await self.config.guild(message.guild).should_delete()
        log_channel = await self.config.guild(message.guild).log_channel()
        public_alerts = await self.config.guild(message.guild).public_alerts()

        if should_delete:
            try:
                await message.delete()
            except discord.errors.Forbidden:
                pass

        if log_channel is not None:
            embed = discord.Embed(title="Phishing Attempt Blocked", color=discord.Color.red())
            embed.add_field(name="User Information", value=f"**Sent By:**{message.author}\n**ID:** {message.author.id}", inline=False)
            embed.add_field(
                inline=False,
                name="Message Content",
                value=f"{message.clean_content}\n\n"
                      f"\N{LINK SYMBOL} [Quick Jump]({message.jump_url})"
            )
            await message.guild.get_channel(log_channel).send(embed=embed)

        if public_alerts is not None:
            embed= discord.Embed(title="Phishing Attempt Stopped", description="**BeeHive automatically removed a confirmed dangerous link for the protection of the server!**\n\n`BeeHive automatically scans all uploaded links against a privately maintained database of links that are manually reviewed by BeeHive Staff + Security`\n\n")
            embed.add_field(name="Malicious User", value=f"**User:** {message.author}\n**ID:** {message.author.id}", inline=False)
            embed.add_field(name="Flagged Message", value=f"**This message contains a confirmed malicious link! Do not click any links in the message!**\n\n||{message.clean_content}||", inline=False)
            await message.guild.get_channel(public_alerts).send(embed=embed)
